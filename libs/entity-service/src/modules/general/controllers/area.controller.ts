import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseInterceptors } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";

import { AreaRepository } from "../repositories";
import { AreaNotFoundError } from "../errors";
import { Area } from "../entities";
import { AreaService } from "../services";
import { AreaResponseDto, CreateAreaBodyDto, UpdateAreaBodyDto } from "../dto";

import {
    ParseUtil,
    ListDataDto,
    BaseQueryDto,
    FullQueryDto,
    ErrorResponseDto,
    ListResponseInterceptor,
    OnUndefinedInterceptor,
    WhereQueryDto,
} from "@app/common";
import { AuthHelper, Authorized } from "@app/entity-service/modules/authetication";

@ApiTags("areas")
@Controller("/areas")
@ApiResponse({ type: ErrorResponseDto, status: 401 })
@ApiResponse({ type: ErrorResponseDto, status: 404 })
@ApiResponse({ type: ErrorResponseDto, status: 500 })
export class AreaController {
    constructor(
        private areaService: AreaService,
        private parseUtil: ParseUtil,
        private areaRepository: AreaRepository,
    ) { }

    @Get()
    @ApiOperation({
        description: `
        example order: {"createdAt": -1}, 
        example select: ["id", "name", "code", "totalProductObject"], 
        example where: {"entityID": "uuid example"} (need entityID to check auth owner entity),
        example relations: ["areaMedias", "productObjects"], 
        type: 0: 'field area', 1: 'farming area', 2: 'production area', 3: 'field & product area', 4: 'field & production area', 5: 'others', 
        status: 0: 'activated', 1: 'deactivated', 
        type of media: 0: 'image', 1: 'video', 2: 'document'`,
    })
    @UseInterceptors(ListResponseInterceptor)
    @OnUndefinedInterceptor(AreaNotFoundError)
    @ApiResponse({ type: AreaResponseDto, isArray: true, status: 200 })
    public find(
        @Query() query: FullQueryDto,
        // @CurrentUser() user: any
    ): Promise<ListDataDto<Area> | undefined> {
        const queryParse = this.parseUtil.fullQueryParam(query);
        return this.areaService.find(queryParse);
    }

    @Get("count")
    @ApiOperation({
        description: `
        example where: {"entityID": "uuid example"} (need entityID to check auth owner entity)`,
    })
    @OnUndefinedInterceptor(AreaNotFoundError)
    @ApiResponse({ type: Object, status: 200 })
    public count(@Query() query: WhereQueryDto): Promise<object | undefined> {
        const queryParse = this.parseUtil.fullQueryParam(query);
        return this.areaService.count(queryParse);
    }

    @Authorized()
    @Post()
    @ApiOperation({
        description: `
        fields optional: employeeID, locationID, medias, code, type, description, address, latitude, longitude,
        totalProductObject, createdAt, updatedAt, deletedAt, status,
        type: 0: 'field area', 1: 'farming area', 2: 'production area', 3: 'field & product area', 4: 'field & production area', 5: 'others', 
        type of media: 0: 'image', 1: 'video', 2: 'document'`,
    })
    @OnUndefinedInterceptor(AreaNotFoundError)
    @ApiResponse({ type: AreaResponseDto, status: 200 })
    public async create(
        @Body() body: CreateAreaBodyDto,
    ): Promise<Area | undefined> {
        return this.areaService.create(body);
    }

    @Get("/:id")
    @ApiOperation({
        description: `
        example select: ["id", "name", "code", "totalProductObject"], 
        example relations: ["areaMedias", "entity", "location"], 
        type: 0: 'field area', 1: 'farming area', 2: 'production area', 3: 'field & product area', 4: 'field & production area', 5: 'others', 
        status: 0: 'activated', 1: 'deactivated', 
        type of media: 0: 'image', 1: 'video', 2: 'document'`,
    })
    @OnUndefinedInterceptor(AreaNotFoundError)
    @ApiResponse({ type: AreaResponseDto, status: 200 })
    public async findOne(
        @Param("id") id: string,
        @Query() query: BaseQueryDto,
        // @CurrentUser() user: any,
    ): Promise<Area | undefined> {
        const queryParse = this.parseUtil.baseQueryParam(query);
        return this.areaService.findOne(id, queryParse);
    }

    @Authorized()
    @Patch("/:id")
    @OnUndefinedInterceptor(AreaNotFoundError)
    @ApiResponse({ type: AreaResponseDto, status: 200 })
    public async update(
        @Param("id") id: string,
        @Body() body: UpdateAreaBodyDto,
        // @CurrentUser() user: any,
    ): Promise<Area | undefined> {
        const bodyParse = this.parseUtil.removeUndefinedProperty(body);

        return this.areaService.update(id, bodyParse);
    }

    @Authorized()
    @Delete("/:id")
    @OnUndefinedInterceptor(AreaNotFoundError)
    public async delete(
        @Param("id") id: string,
    ): Promise<Area | undefined> {

        return this.areaService.delete(id);
    }
}
