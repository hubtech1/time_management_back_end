import { HttpException, HttpStatus, Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request, Response, NextFunction } from 'express';
import { StaffService } from '../../services';

@Injectable()
export class AuthenticateMiddleware implements NestMiddleware {
    constructor(
        public jwtService: JwtService,
        private readonly staffService: StaffService
    ) { }
    async use(req: Request, res: Response, next: NextFunction) {
        const token = req.header("token")

        if (!token) {
            throw new UnauthorizedException(`No have token`);
        }
        const decode = await this.jwtService.verify(token, { secret: process.env.TOKEN_SECRET_KEY })
        // console.log("decode middleware", decode);

        const staff = await this.staffService.findStaffCode(decode.staffCode)
        // console.log("staff middleware", staff);

        if (!staff) {
            throw new UnauthorizedException("Staff not found!!");
        }

        if (!decode) {
            throw new UnauthorizedException("Token không đúng");
        }
        req.user = decode
        // console.log("AuthenticateMiddleware");
        // console.log("AuthenticateMiddleware req", req.user);

        next();
    }
}




