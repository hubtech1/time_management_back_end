import { forwardRef, MiddlewareConsumer, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { Area, Staff, TimeSheet, Department, Date, Week } from "./entities";
import { AreaRepository, StaffRepository, TimeSheetRepository, DepartmentRepository, DateRepository, WeekRepository } from "./repositories";
import { AreaService, StaffService, TimeSheetService, DepartmentService, DateService, WeekService } from "./services";
import { AreaController, StaffController, TimeSheetController, DepartmentController, DateController, WeekController } from "./controllers";

import { CommonModule } from "@app/common";
import { JwtModule } from "@nestjs/jwt";
import { AuthenticateMiddleware } from "./middleware/auth/authenticate.middleware";
import { AuthorizeMiddleware } from "./middleware/auth/authorization.middleware";

const SERVICES = [AreaService, StaffService, TimeSheetService, DepartmentService, DateService, WeekService];

@Module({
    imports: [
        TypeOrmModule.forFeature([Area, AreaRepository, Staff, StaffRepository, TimeSheet, TimeSheetRepository, Department, Date, DepartmentRepository, DateRepository, Week, WeekRepository]),
        JwtModule.register({
            secret: process.env.TOKEN_SECRET_KEY || "STAFF_TIME_MANAGEMENT",
            signOptions: { expiresIn: 365 * 24 * 60 * 60 },
        }),

        forwardRef(() => CommonModule),
    ],
    providers: [...SERVICES],
    exports: [...SERVICES, TypeOrmModule],
    controllers: [AreaController, StaffController, TimeSheetController, DepartmentController, DateController, WeekController],
})
export class GeneralModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(AuthenticateMiddleware, AuthorizeMiddleware)
            .forRoutes('staffs');
    }
}
