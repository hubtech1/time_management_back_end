import { Module } from "@nestjs/common";

import { GeneralModule } from "./modules/general";

@Module({
    imports: [GeneralModule,],
    providers: [],
    exports: [GeneralModule],
})
export class EntityServiceModule { }
