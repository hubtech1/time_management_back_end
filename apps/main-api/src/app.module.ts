import { ClassSerializerInterceptor, Module } from "@nestjs/common";
import { APP_FILTER, APP_INTERCEPTOR } from "@nestjs/core";

import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { TypeormLoader } from "./loaders/typeormLoader";
import { WinstonLoader } from "./loaders/winstonLoader";
import { CacheStoreLoader } from "./loaders/cacheStoreLoader";
import { GraphqlLoader } from "./loaders/graphqlLoader";
import { AppResolver } from "./app.resolver";

import { EntityServiceModule } from "@app/entity-service";
import { LogForbiddenExceptionFilter, ResponseInterceptor } from "@app/common";

const LOADERS = [TypeormLoader, WinstonLoader, CacheStoreLoader.register(), GraphqlLoader.load()];

@Module({
    imports: [...LOADERS, EntityServiceModule],
    controllers: [AppController],
    providers: [
        AppService,
        AppResolver,
        {
            provide: APP_INTERCEPTOR,
            useClass: ClassSerializerInterceptor,
        },
        {
            provide: APP_INTERCEPTOR,
            useClass: ResponseInterceptor,
        },
        {
            provide: APP_FILTER,
            useClass: LogForbiddenExceptionFilter,
        },
    ],
})
export class AppModule {}
