import { registerDecorator, ValidationArguments, ValidationOptions } from "class-validator";
import moment from "moment";

export function IsCorrectStartedAt(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "IsCorrectStartedAt",
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    return (
                        typeof value === "string" && (moment().isBefore(value, "day") || moment().isSame(value, "day"))
                    );
                },
                defaultMessage(args: ValidationArguments): string {
                    return `${args.property} is before today!`;
                },
            },
        });
    };
}

export function IsFollowStandardID(property: string, validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "IsCorrectStartedAt",
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const standardID = value[0][property];

                    // compare with standard ID and find in exception case
                    const indexException = value.findIndex((e) => e[property] !== standardID);

                    return indexException === -1;
                },
                defaultMessage(args: ValidationArguments): string {
                    return `${args.property} must have ${property} similar each item!`;
                },
            },
        });
    };
}

export function IsCorrectFinishedAt(property: string, validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "IsCorrectFinishedAt",
            target: object.constructor,
            propertyName: propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const [relatedPropertyName] = args.constraints;
                    const relatedValue = (args.object as any)[relatedPropertyName];
                    return typeof value === "string" && typeof relatedValue === "string" && value >= relatedValue;
                },
                defaultMessage(args: ValidationArguments): string {
                    return `StartedAt > ${args.property}!`;
                },
            },
        });
    };
}

function isValidURL(value: string) {
    const pattern = new RegExp(
        "^(https?:\\/\\/)?" + // protocol
            "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
            "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
            "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
            "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
            "(\\#[-a-z\\d_]*)?$",
        "i",
    ); // fragment locator
    return !!pattern.test(value);
}

function isValidNumber(value: any) {
    return !isNaN(value);
}

function isImage(value: string) {
    const formats: string[] = [".jpg", ".JPG", ".png", ".PNG", ".jpeg", ".JPEG"];

    formats.forEach((e) => {
        const index = value.search(e);

        if (index !== -1) {
            return true;
        }
    });

    return false;
}

function isVideo(value: string) {
    const formats: string[] = [".mp4", ".MP4"];

    formats.forEach((e) => {
        const index = value.search(e);

        if (index !== -1) {
            return true;
        }
    });

    return false;
}

function isDocument(value: string) {
    const formats: string[] = [
        ".docs",
        ".DOCS",
        ".xlsx",
        ".XLSX",
        ".pdf",
        ".PDF",
        ".csv",
        ".CSV",
        ".tsv",
        ".TSV",
        ".html",
        ".HTML",
    ];

    formats.forEach((e) => {
        const index = value.search(e);

        if (index !== -1) {
            return true;
        }
    });

    return false;
}

export function IsValidStepPropertyValue(property: string, validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "IsValidStepPropertyValue",
            target: object.constructor,
            propertyName: propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const [relatedPropertyName] = args.constraints;
                    const type = (args.object as any)[relatedPropertyName];

                    switch (type) {
                        case 1:
                            return isValidNumber(value);
                        case 2:
                            return isValidURL(value);
                        default:
                            return typeof value === "string";
                    }
                },
                defaultMessage(args: ValidationArguments): string {
                    return `${args.property} is invalid step property value!`;
                },
            },
        });
    };
}

export function IsValidTypeMediaValue(property: string, validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: "IsValidTypeMediaValue",
            target: object.constructor,
            propertyName: propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const [relatedPropertyName] = args.constraints;
                    const type = (args.object as any)[relatedPropertyName];

                    switch (type) {
                        case 1:
                            return isVideo(value);
                        case 2:
                            return isDocument(value);
                        default:
                            return isImage(value);
                    }
                },
                defaultMessage(args: ValidationArguments): string {
                    return `${args.property} is invalid type media value!`;
                },
            },
        });
    };
}
