import { EntityRepository, Repository } from "typeorm";
import { Department } from "../entities";



@EntityRepository(Department)
export class DepartmentRepository extends Repository<Department> {

}