import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateDateTable1655977647801 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: "date",
            columns: [
                {
                    name: "id",
                    type: "uuid",
                    isPrimary: true,
                    isNullable: false
                },
                {
                    name: "week_id",
                    type: "uuid",
                    isNullable: false
                },
                {
                    name: "date",
                    type: "timestamp",
                    isNullable: false
                },

            ],
        });

        await queryRunner.createTable(table);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("date");
    }

}
