import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateAreaTable1621775613006 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: "area",
            columns: [
                {
                    name: "id",
                    type: "uuid",
                    isPrimary: true,
                    isNullable: false,
                },
                {
                    name: "owner_id",
                    type: "uuid",
                    isNullable: false,
                },
                {
                    name: "entity_id",
                    type: "uuid",
                    isNullable: true,
                },
                {
                    name: "location_id",
                    type: "uuid",
                    isNullable: true,
                },
                {
                    name: "short_id",
                    type: "varchar",
                    isNullable: false,
                    isUnique: true,
                },
                {
                    name: "code",
                    type: "varchar",
                    isNullable: false,
                },
                {
                    name: "total_area",
                    type: "int",
                    isNullable: true,
                    default: 0,
                },
                {
                    name: "total_quantity",
                    type: "int",
                    isNullable: true,
                    default: 0,
                },
                {
                    name: "avatar",
                    type: "varchar",
                    isNullable: false,
                },
                {
                    name: "avatar_thumbnail",
                    type: "varchar",
                    isNullable: false,
                },
                {
                    name: "total_product_object",
                    type: "smallint",
                    isNullable: false,
                    default: 0,
                },
                {
                    name: "name",
                    type: "varchar",
                    isNullable: false,
                },
                // 0: 'field area', 1: 'farming area', 2: 'production area', 3: 'field & product area', 4: 'field & production area', 5: 'others'
                {
                    name: "type",
                    type: "smallint",
                    isNullable: false,
                    default: 0,
                },
                {
                    name: "description",
                    type: "text",
                    isNullable: true,
                },
                {
                    name: "address",
                    type: "text",
                    isNullable: true,
                },
                {
                    name: "latitude",
                    type: "varchar",
                    isNullable: true,
                },
                {
                    name: "longitude",
                    type: "varchar",
                    isNullable: true,
                },
                // 0: activated, 1: deactivated, 2: paused, 3: drafted
                {
                    name: "status",
                    type: "smallint",
                    isNullable: false,
                    default: 0,
                },
                {
                    name: "created_at",
                    type: "timestamp",
                    isNullable: false,
                    default: "now()",
                },
                {
                    name: "updated_at",
                    type: "timestamp",
                    isNullable: false,
                    default: "now()",
                },
                {
                    name: "deleted_at",
                    type: "timestamp",
                    isNullable: true,
                    default: undefined,
                },
                {
                    name: "full_text_search_col",
                    type: "varchar",
                    isNullable: false,
                },
            ],
        });

        await queryRunner.createTable(table);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("area");
    }
}
