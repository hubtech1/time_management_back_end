import { createParamDecorator, ExecutionContext, ForbiddenException } from "@nestjs/common";
import { Request } from "express";

import { AuthHelper } from "./auth.helper";

interface CurrentUserOptions {
    required?: boolean;
}

export function CurrentUser({ required = false }: CurrentUserOptions = {}) {
    return createParamDecorator((data, context: ExecutionContext) => {
        const request = context.switchToHttp().getRequest<Request>();
        const authorization = request.headers.authorization;

        if (authorization && authorization.startsWith("Bearer ")) {
            const [, token] = authorization.split(" ", 2);

            const user = AuthHelper.getPayloadFromJWT(token);
            user.id = user.sub;
            if (!!required && !user) {
                throw new ForbiddenException();
            }
            return user;
        }

        return;
    })();
}
