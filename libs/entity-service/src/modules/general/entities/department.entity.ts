import { Column, Entity as EntityTypeORM, OneToMany } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";

import { Base } from "@app/common/entities";
import { Staff } from "./staff.entity";


@EntityTypeORM()
@ObjectType()
export class Department extends Base {
    @Column({ name: "department_code", nullable: false })
    @Field()
    public departmentCode: number;

    @Column({ name: "department_name", nullable: false })
    @Field()
    public departmentName: string;

    @OneToMany(() => Staff, staff => staff.departmentId)
    public departmentId: string

}