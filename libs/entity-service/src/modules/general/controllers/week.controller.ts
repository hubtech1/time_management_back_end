import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, UseInterceptors } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";


import { WeekRepository } from "../repositories";
import { WeekNotFoundError } from "../errors";
import { WeekService } from "../services";

import {
    ParseUtil,
    ListDataDto,
    BaseQueryDto,
    FullQueryDto,
    ErrorResponseDto,
    ListResponseInterceptor,
    OnUndefinedInterceptor,
    WhereQueryDto,
} from "@app/common";
import { CreateWeekBodyDto, WeekResponseDto, UpdateWeekBodyDto } from "../dto";
import { Week } from "../entities";
import { Authorized } from "type-graphql";
import { IndexMetadata } from "typeorm/metadata/IndexMetadata";


@ApiTags("weeks")
@Controller("/weeks")
@ApiResponse({ type: ErrorResponseDto, status: 401 })
@ApiResponse({ type: ErrorResponseDto, status: 404 })
@ApiResponse({ type: ErrorResponseDto, status: 500 })
export class WeekController {
    constructor(
        private weekService: WeekService,
        private parseUtil: ParseUtil,
        private weekRepository: WeekRepository,

    ) { }

    // Lấy hết danh sách Week
    @Get()
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated', 
        `
    })
    @UseInterceptors(ListResponseInterceptor)
    @OnUndefinedInterceptor(WeekNotFoundError)
    @ApiResponse({ type: WeekResponseDto, isArray: true, status: 200 })
    public find(
        @Query() query: FullQueryDto,

    ): Promise<ListDataDto<Week> | undefined> {
        const queryParse = this.parseUtil.fullQueryParam(query)
        return this.weekService.find(queryParse)
    }

    // Tạo thêm Week
    // @Authorized()
    @Post()
    @ApiOperation({
        description: `
        role: 0: 'staff', 1: 'manager', 2: 'director'`,
    })
    @OnUndefinedInterceptor(WeekNotFoundError)
    @ApiResponse({ type: WeekResponseDto, status: 200 })
    public async create(
        @Body() body: CreateWeekBodyDto,
    ): Promise<Week | undefined> {
        return this.weekService.create(body)
    }

    // Tìm 1 Week theo id
    @Get("/:id")
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated',
        `
    })
    @OnUndefinedInterceptor(WeekNotFoundError)
    @ApiResponse({ type: WeekResponseDto, status: 200 })
    public async findOne(
        @Param("id") id: string,
        @Query() query: BaseQueryDto,
    ): Promise<Week | undefined> {
        const queryParse = this.parseUtil.baseQueryParam(query);
        return this.weekService.findOne(id, queryParse)
    }

    // Update thông tin Week

    @Authorized()
    @Patch("/:id")
    @OnUndefinedInterceptor(WeekNotFoundError)
    @ApiResponse({ type: WeekResponseDto, status: 200 })
    public async update(
        @Param("id") id: string,
        @Body() body: UpdateWeekBodyDto
    ): Promise<Week | undefined> {
        const bodyParse = this.parseUtil.removeUndefinedProperty(body);
        return this.weekService.update(id, bodyParse)
    }

    // Xóa Week
    @Authorized()
    @Delete("/:id")
    @OnUndefinedInterceptor(WeekNotFoundError)
    public async delete(
        @Param("id") id: string,
    ): Promise<Week | undefined> {
        return this.weekService.delete(id)
    }













}