import _ from "lodash";
import * as uuid from "uuid";
import { Injectable } from "@nestjs/common";

import { WeekRepository } from "../repositories";

import { DBUtil, Logger, ListDataDto, CrudOption, env, CommonUtil } from "@app/common";
import { CreateWeekBodyDto, UpdateWeekBodyDto } from "@app/entity-service/modules/general/dto";
import { Week } from "../entities";


@Injectable()
export class WeekService {
    constructor(
        private weekRepository: WeekRepository,
        private dbUtil: DBUtil,
        private commonUtil: CommonUtil,
        private logger: Logger
    ) { }

    public async find(option: CrudOption = {}): Promise<ListDataDto<Week> | undefined> {
        this.logger.info("Find all Week");
        return this.dbUtil.findAndCount(this.weekRepository, option);
    }

    public async create(
        body: CreateWeekBodyDto,
        option: CrudOption = {},
    ): Promise<Week> {
        this.logger.info("Create a new Week");

        const week = new Week;
        _.assign(week, body);
        week.id = uuid.v1()

        return this.weekRepository.save(week)
    }

    public async findOne(id: string, option: CrudOption = {}): Promise<Week> {
        this.logger.info("Find one Week");

        const result = await this.dbUtil.findOne(this.weekRepository, id, option);

        if (!result) {
            return result
        }

        return result
    }

    public async update(id: string, body: UpdateWeekBodyDto, option?: CrudOption): Promise<Week | undefined> {
        this.logger.info("Update some fields a Week");

        await this.weekRepository.update(id, body);

        return this.weekRepository.findOne(id)
    }

    public async delete(id: string, option?: CrudOption): Promise<Week | undefined> {
        this.logger.info("Delete a Week");
        const item = await this.weekRepository.findOne(id);
        if (item === undefined) {
            return undefined
        } else {
            await this.weekRepository.delete(id)
        }
        return item
    }
}