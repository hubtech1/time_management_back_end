import { MigrationInterface, QueryRunner, TableForeignKey } from "typeorm";

export class AddRelationForStaffTable1655978065877 implements MigrationInterface {

    private toDepartment = new TableForeignKey({
        name: "fk_staff_department",
        columnNames: ["department_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "department",
    })

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createForeignKey("staff", this.toDepartment)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey("staff", this.toDepartment)
    }

}
