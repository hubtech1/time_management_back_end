# Introduction

Document website: [https://farmhub2.gitlab.io/backend/farm-service/](https://farmhub2.gitlab.io/backend/farm-service/)

-   HOME

    -   [Home](README.md)

-   Typeorm

    -   [Custom query](typeorm/custom-query.md)
    -   [Condition parser](typeorm/condition-parser.md)

-   Caching

    -   [Redis setup](caching/redis.md)
    -   [Request caching](caching/request-caching.md)
    -   [Database caching](caching/database-caching.md)

-   GraphQL

    -   [Setup](graphql/setup.md)
    -   [Using with Models](graphql/models.md)
    -   [Write Resolver](graphql/resolver.md)

-   Blockchain
    -   [Tendermint API](blockchain/tendermint-api.md)
    -   [Tendermint Integration](blockchain/tendermint-integration.md)

---

| Created by [Docsify](https://docsify.js.org) |
| -------------------------------------------: |
