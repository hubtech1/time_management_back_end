import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { map, Observable } from "rxjs";
import { isEmpty } from "class-validator";

import { PaginateJsonResponse } from "@app/common/interceptors/list-response.interceptor";

export class JsonResponse<T> {
    code: number;
    data: T;
}

@Injectable()
export class ResponseInterceptor<T extends any = any>
    implements NestInterceptor<T, JsonResponse<T> | PaginateJsonResponse<T>>
{
    public intercept(
        context: ExecutionContext,
        next: CallHandler,
    ): Observable<JsonResponse<T> | PaginateJsonResponse<T>> {
        return next.handle().pipe(
            map((content: T) => {
                if (content === undefined || content === null || isEmpty(content)) {
                    return { code: 404, data: undefined };
                }

                if (Object.keys(content).length === 0) {
                    return { code: 200, data: undefined };
                }

                if (typeof content === "object" && Reflect.has(content as object, "pagination")) {
                    return {
                        code: 200,
                        // @ts-ignore
                        data: content.data,
                        // @ts-ignore
                        pagination: content.pagination,
                    };
                }

                return { code: 200, data: content };
            }),
        );
    }
}
