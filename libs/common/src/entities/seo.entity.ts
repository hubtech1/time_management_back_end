import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class SEO {
    @Field((type) => String, { nullable: true })
    public icon: string;

    @Field((type) => String, { nullable: true })
    public banner: string;

    @Field((type) => String, { nullable: true })
    public title: string;

    @Field((type) => String, { nullable: true })
    public description: string;
}
