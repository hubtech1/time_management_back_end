import { Query, Resolver } from "@nestjs/graphql";
import { Injectable } from "@nestjs/common";
import { GraphQLJSONObject } from "graphql-type-json";

import { AppService } from "./app.service";

@Resolver()
@Injectable()
export class AppResolver {
    constructor(private readonly appService: AppService) {}

    @Query(() => GraphQLJSONObject)
    public info() {
        return this.appService.getHello();
    }
}
