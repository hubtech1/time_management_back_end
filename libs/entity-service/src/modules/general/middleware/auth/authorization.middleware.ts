import { HttpException, HttpStatus, Injectable, NestMiddleware, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Request, Response, NextFunction } from 'express';
import { MiddlewareFn } from 'type-graphql';
import { StaffService } from '../../services';

@Injectable()
export class AuthorizeMiddleware implements NestMiddleware {
    constructor(
        public jwtService: JwtService,
        private readonly staffService: StaffService
    ) { }
    async use(req: any, res: Response, next: NextFunction) {
        const { user, body } = req
        console.log("AuthorizeMiddleware", req.user.role);
        console.log("AuthorizeMiddleware", req.body);

        switch (user.role) {
            case 0: {
                next()

            } break;
            case 1: {
                next()

            } break;
            case 2: {
                next()
            } break;
            default:
                throw new UnauthorizedException("Bạn cần đăng nhập");
        }


        // console.log("AuthorizeMiddleware");
        // console.log("AuthorizeMiddleware req", req.body);

        next();
    }
}


