import _ from "lodash";
import { FindOneOptions, Like, QueryRunner } from "typeorm";
import * as uuid from "uuid";
import { Injectable } from "@nestjs/common";

import { Area } from "../entities";
import { AreaRepository } from "../repositories";

import { DBUtil, Logger, ListDataDto, CrudOption, env, CommonUtil } from "@app/common";
import { CreateAreaBodyDto, UpdateAreaBodyDto } from "@app/entity-service/modules/general/dto";

@Injectable()
export class AreaService {
    constructor(
        private areaRepository: AreaRepository,
        private dbUtil: DBUtil,
        private commonUtil: CommonUtil,
        private logger: Logger,
    ) { }

    public async getValidCodeInTransaction(queryRunner: QueryRunner, area: Area): Promise<string> {
        const prefixCode = await DBUtil.combineFirstCharacterAndLastWord(area.name);
        const findOneOptions: FindOneOptions<Area> = {
            where: { code: Like(`${prefixCode}%`) },
            order: { createdAt: "DESC" },
            select: ["code"],
        };

        return this.dbUtil.getValidCodeInTransaction(queryRunner, Area, findOneOptions, prefixCode, 4);
    }

    public async find(option: CrudOption = {}): Promise<ListDataDto<Area> | undefined> {
        this.logger.info("Find all area");
        return this.dbUtil.findAndCount(this.areaRepository, option);
    }

    public async count(option: CrudOption = {}): Promise<Object | undefined> {
        this.logger.info("Count all area");
        const size = this.dbUtil.count(this.areaRepository, option);

        return { size };
    }

    public async create(
        body: CreateAreaBodyDto,
        option: CrudOption = {},
    ): Promise<Area> {
        this.logger.info("Create a new area");

        const area = new Area();
        _.assign(area, body);
        area.id = uuid.v1();
        area.shortID = this.commonUtil.genShortID();
        area.totalProductObject = 0;
        area.avatar = area.avatar ?? env.defaultValue.defaultAvatar;
        area.avatarThumbnail = area.avatarThumbnail ?? env.defaultValue.defaultAvatar;

        return this.areaRepository.save(area);
    }

    public async findOne(id: string, option: CrudOption = {}): Promise<Area> {
        this.logger.info("Find one area");

        const result = await this.dbUtil.findOne(this.areaRepository, id, option);

        if (!result) {
            return result;
        }

        return result;
    }

    public async update(id: string, body: UpdateAreaBodyDto, option?: CrudOption): Promise<Area | undefined> {
        this.logger.info("Update some fields a area");

        await this.areaRepository.update(id, body);
        return this.areaRepository.findOne(id);
    }

    public async delete(id: string, option?: CrudOption): Promise<Area | undefined> {
        this.logger.info("Delete a area");
        const item = await this.areaRepository.findOne(id);
        if (item === undefined) {
            return undefined;
        }
        await this.areaRepository.delete(id);

        return item;
    }

    // public async delete2(id: string, option?: CrudOption): Promise<Area | undefined> {
    //     this.logger.info("Delete a area");
    //     const item = await this.areaRepository.findOne(id);
    //     if (item === undefined) {
    //         return undefined;
    //     }
    //     await this.areaRepository.getInactiveUsers();

    //     return item;
    // }


}
