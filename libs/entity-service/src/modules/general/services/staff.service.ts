import _ from "lodash";
import * as uuid from "uuid";
import { HttpException, Injectable } from "@nestjs/common";

import { DepartmentRepository, StaffRepository } from "../repositories";
import * as bcrypt from 'bcrypt';
import { DBUtil, Logger, ListDataDto, CrudOption, env, CommonUtil } from "@app/common";
import { CreateStaffBodyDto, UpdateStaffBodyDto } from "@app/entity-service/modules/general/dto";
import { Staff } from "../entities";
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class StaffService {
    constructor(
        private staffRepository: StaffRepository,
        private dbUtil: DBUtil,
        private commonUtil: CommonUtil,
        private logger: Logger,
        private departmentRepository: DepartmentRepository,
        private jwtService: JwtService
    ) { }

    public async find(option: CrudOption = {}): Promise<ListDataDto<Staff> | undefined> {
        this.logger.info("Find all Staff");

        return this.dbUtil.findAndCount(this.staffRepository, option);
    }

    public async create(
        body: CreateStaffBodyDto,
        option: CrudOption = {},
    ): Promise<Staff> {
        this.logger.info("Create a new Staff");
        // Tạo 1 chuỗi salt ngẫu nhiên
        const salt = bcrypt.genSaltSync(10);
        // mã hóa salt + password
        const hashPassword = bcrypt.hashSync(body.password, salt)

        // lấy dữ liệu từ department để gán Id
        const departmentList = await this.departmentRepository.find()
        const randomNumber = Math.floor(Math.random() * 12)

        const staff = new Staff;
        _.assign(staff, body);
        staff.id = uuid.v1();
        staff.departmentId = departmentList[randomNumber].id
        staff.avatar = staff.avatar ?? env.defaultValue.defaultAvatar;
        staff.password = hashPassword
        return this.staffRepository.save(staff)
    }

    public async validateStaff(staffCode: string, password: string) {
        this.logger.info("Log in staff");

        const allStaff = await this.staffRepository.find();

        // Tìm user có trong list user ko
        const index = allStaff.findIndex(staff => staff.staffCode === staffCode)
        if (index === -1) {
            return "Không tìm thấy Staff code"
        }

        // Kiểm tra mật khẩu có đúng hay ko
        const matchPass = bcrypt.compareSync(password, allStaff[index].password)

        if (matchPass) {
            const { password, ...rest } = allStaff[index];
            const payload = {
                staffCode: allStaff[index].staffCode,
                role: allStaff[index].role,
                email: allStaff[index].email
            }
            const access_token = this.jwtService.sign(
                payload,
            )

            return { ...payload, access_token }
        }
        return "Password không đúng"
    }

    public async findOne(id: string, option: CrudOption = {}): Promise<Staff> {
        this.logger.info("Find one Staff");

        const result = await this.dbUtil.findOne(this.staffRepository, id, option);

        if (!result) {
            return result
        }

        return result
    }

    public async findStaffCode(staffCode: string): Promise<any | undefined> {
        this.logger.info("Find one Staff by Staff Code");

        const staff = await this.staffRepository.findOne({
            where: { staffCode: staffCode },
        });

        if (staff === undefined) {
            return undefined
        } else {
            return staff
        }
    }

    public async update(staffCode: string, body: UpdateStaffBodyDto, option?: CrudOption): Promise<Staff | undefined> {
        this.logger.info("Update some fields a Staff");
        // check user có tồn tại hay không
        const currentStaff = await this.staffRepository.findOne({
            where: { staffCode: staffCode },
        });


        if (currentStaff) {
            // hash password trước khi update
            const hashPassword = await this.staffRepository.hashPassword(body.password)

            await this.staffRepository.update(currentStaff.id, { ...body, password: hashPassword });

            return this.staffRepository.findOne(currentStaff.id)
        } else {
            throw new HttpException("Staff not found", 404)
        }
    }

    public async delete(staffCode: string, option?: CrudOption): Promise<Staff | undefined> {
        this.logger.info("Delete a Staff");
        const item = await this.staffRepository.findOne({
            where: { staffCode: staffCode },
        });

        if (item === undefined) {
            return undefined
        } else {
            await this.staffRepository.delete(item.id)
        }
        return item
    }
}