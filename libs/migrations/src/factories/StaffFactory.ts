import * as Faker from "faker";
import { define } from "typeorm-seeding";
import * as uuid from "uuid";

import { Staff } from "@app/entity-service/modules/general/entities";


define(
    Staff,
    (
        faker: typeof Faker,
    ) => {
        const staff = new Staff();

        staff.id = uuid.v1();
        staff.staffCode = faker.lorem.word();
        staff.fullName = faker.lorem.word();

        staff.role = faker.random.number({ min: 0, max: 2 });
        staff.avatar = faker.lorem.lines();
        staff.password = "testpassword123";
        staff.email = faker.name.lastName();

        return staff
    }
)