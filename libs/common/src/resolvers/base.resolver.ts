import { Args, ArgsType, Field, Query, Resolver } from "@nestjs/graphql";
import { Repository } from "typeorm";
import { plural } from "pluralize";
import { GraphQLJSON } from "graphql-type-json";
import { Injectable } from "@nestjs/common";
import { ClassConstructor } from "class-transformer";
import { ClassType } from "type-graphql";
import { InjectRepository } from "@nestjs/typeorm";

import { ParseUtil, ConditionQueryBuilder } from "../utils";

import { CrudOption } from "@app/common/interfaces/common.interface";
import { FullQueryDto } from "@app/common/dto/query.dto";

@ArgsType()
export class ExtendedQuery extends FullQueryDto {
    @Field(() => GraphQLJSON, { nullable: true })
    public where: any;

    @Field(() => GraphQLJSON, { nullable: true })
    public order: any;
}

export interface IResolver<T> {
    findAll(query: ExtendedQuery): Promise<T[]>;
    findOne(id: string): Promise<T>;
}

export function BaseResolver<T>(TClassFunc: () => ClassType<T>, name: string): ClassConstructor<IResolver<T>> {
    const TClass = TClassFunc();

    @Injectable()
    @Resolver({ isAbstract: true })
    abstract class AbstractResolver implements IResolver<T> {
        // public readonly repository: Repository<T>;

        protected constructor(
            public readonly parseUtil: ParseUtil,
            @InjectRepository(TClass)
            public readonly repository: Repository<T>,
        ) {}

        @Query(() => [TClass], { name: plural(name) })
        public async findAll(@Args() query: ExtendedQuery): Promise<T[]> {
            const parsedQuery = this.parseUtil.fullQueryParam(query) as CrudOption;
            const [list, count] = await ConditionQueryBuilder.ofRepository(this.repository)
                .select(undefined)
                .relations([])
                .take(parsedQuery.take)
                .skip(parsedQuery.skip)
                .condition(parsedQuery.where)
                .order(query.order)
                .build()
                .getManyAndCount();
            return list;
        }

        @Query(() => TClass, { name })
        public async findOne(@Args("id") id: string): Promise<T> {
            return await this.repository.findOne(id);
        }
    }

    // @ts-ignore
    return AbstractResolver;
}
