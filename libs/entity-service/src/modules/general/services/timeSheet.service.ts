import _ from "lodash";
import * as uuid from "uuid";
import { Injectable } from "@nestjs/common";

import { TimeSheetRepository } from "../repositories";

import { DBUtil, Logger, ListDataDto, CrudOption, env, CommonUtil } from "@app/common";
import { CreateTimeSheetBodyDto, UpdateTimeSheetBodyDto } from "@app/entity-service/modules/general/dto";
import { TimeSheet } from "../entities";


@Injectable()
export class TimeSheetService {
    constructor(
        private timeSheetRepository: TimeSheetRepository,
        private dbUtil: DBUtil,
        private commonUtil: CommonUtil,
        private logger: Logger
    ) { }

    public async find(option: CrudOption = {}): Promise<ListDataDto<TimeSheet> | undefined> {
        this.logger.info("Find all TimeSheet");
        return this.dbUtil.findAndCount(this.timeSheetRepository, option);
    }

    public async create(
        body: CreateTimeSheetBodyDto,
        option: CrudOption = {},
    ): Promise<TimeSheet> {
        this.logger.info("Create a new TimeSheet");

        const timeSheet = new TimeSheet;
        _.assign(timeSheet, body);
        timeSheet.id = uuid.v1()

        return this.timeSheetRepository.save(timeSheet)
    }

    public async findOne(id: string, option: CrudOption = {}): Promise<TimeSheet> {
        this.logger.info("Find one TimeSheet");

        const result = await this.dbUtil.findOne(this.timeSheetRepository, id, option);

        if (!result) {
            return result
        }

        return result
    }

    public async update(id: string, body: UpdateTimeSheetBodyDto, option?: CrudOption): Promise<TimeSheet | undefined> {
        this.logger.info("Update some fields a TimeSheet");

        await this.timeSheetRepository.update(id, body);

        return this.timeSheetRepository.findOne(id)
    }

    public async delete(id: string, option?: CrudOption): Promise<TimeSheet | undefined> {
        this.logger.info("Delete a TimeSheet");
        const item = await this.timeSheetRepository.findOne(id);
        if (item === undefined) {
            return undefined
        } else {
            await this.timeSheetRepository.delete(id)
        }
        return item
    }
}