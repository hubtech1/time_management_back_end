FROM node:14-alpine3.10

RUN apk add python make g++

# Create work directory
WORKDIR /app

# Copy app source to work directory
COPY package.json .
COPY yarn.lock .
RUN yarn install
COPY . .
RUN yarn build

# Install app dependencies
EXPOSE 4000

# Build and run the app
CMD ["yarn", "start:prod"]
