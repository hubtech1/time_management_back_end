import * as Faker from "faker";
import { define } from "typeorm-seeding";
import * as uuid from "uuid";

import { Department } from "@app/entity-service/modules/general/entities";


define(Department, (faker: typeof Faker) => {
    const department = new Department;

    department.id = uuid.v1()
    department.departmentCode = faker.random.number({ min: 0, max: 5 });
    department.departmentName = "Dev" + faker.lorem.word()


    return department
})