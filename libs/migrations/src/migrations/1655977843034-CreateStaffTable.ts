import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateStaffTable1655977843034 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: "staff",
            columns: [
                {
                    name: "id",
                    type: "uuid",
                    isPrimary: true,
                    isNullable: false
                },
                {
                    name: "staff_code",
                    type: "varchar",
                    isNullable: false
                },
                {
                    name: "full_name",
                    type: "varchar",
                    isNullable: false
                },
                {
                    name: "department_id",
                    type: "uuid",
                    isNullable: false
                },
                {
                    name: "role",
                    type: "int",
                    isNullable: false
                },
                {
                    name: "avatar",
                    type: "varchar",
                    isNullable: true
                },
                {
                    name: "password",
                    type: "varchar",
                    isNullable: false
                },
                {
                    name: "email",
                    type: "varchar",
                    isNullable: false
                },
            ],
        });

        await queryRunner.createTable(table);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("staff");

    }

}
