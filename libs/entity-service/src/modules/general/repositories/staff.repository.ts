import { EntityRepository, Repository } from "typeorm";
import { Staff } from "../entities";
import * as bcrypt from 'bcrypt';



@EntityRepository(Staff)
export class StaffRepository extends Repository<Staff> {
    public async hashPassword(basePassword: string) {
        // Tạo 1 chuỗi salt ngẫu nhiên
        const salt = bcrypt.genSaltSync(10);
        // mã hóa salt + password
        const hashPassword = bcrypt.hashSync(basePassword, salt)

        return hashPassword
    }
}