import _ from "lodash";
import * as uuid from "uuid";
import { Injectable } from "@nestjs/common";

import { DateRepository } from "../repositories";

import { DBUtil, Logger, ListDataDto, CrudOption, env, CommonUtil } from "@app/common";
import { CreateDateBodyDto, UpdateDateBodyDto } from "@app/entity-service/modules/general/dto";
import { Date } from "../entities";


@Injectable()
export class DateService {
    constructor(
        private dateRepository: DateRepository,
        private dbUtil: DBUtil,
        private commonUtil: CommonUtil,
        private logger: Logger
    ) { }

    public async find(option: CrudOption = {}): Promise<ListDataDto<Date> | undefined> {
        this.logger.info("Find all Date");
        return this.dbUtil.findAndCount(this.dateRepository, option);
    }

    public async create(
        body: CreateDateBodyDto,
        option: CrudOption = {},
    ): Promise<Date> {
        this.logger.info("Create a new Date");

        const date = new Date;
        _.assign(date, body);
        date.id = uuid.v1()

        return this.dateRepository.save(date)
    }

    public async findOne(id: string, option: CrudOption = {}): Promise<Date> {
        this.logger.info("Find one Date");

        const result = await this.dbUtil.findOne(this.dateRepository, id, option);

        if (!result) {
            return result
        }

        return result
    }

    public async update(id: string, body: UpdateDateBodyDto, option?: CrudOption): Promise<Date | undefined> {
        this.logger.info("Update some fields a Date");

        await this.dateRepository.update(id, body);

        return this.dateRepository.findOne(id)
    }

    public async delete(id: string, option?: CrudOption): Promise<Date | undefined> {
        this.logger.info("Delete a Date");
        const item = await this.dateRepository.findOne(id);
        if (item === undefined) {
            return undefined
        } else {
            await this.dateRepository.delete(id)
        }
        return item
    }
}