import { IsDateString, IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString, IsUrl, IsUUID, Length, Max, Min } from "class-validator";
import { Exclude, Expose } from "class-transformer";
import { OmitType } from "@nestjs/swagger";



@Exclude()
export class BaseWeekDto {
    @Expose()
    @IsNotEmpty()
    public name: string

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;

    @Expose()
    @IsDateString()
    @IsOptional()
    public createdAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public updatedAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public deletedAt: string;



}

@Exclude()
export class WeekResponseDto extends BaseWeekDto {
    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public id: string;
}

export class CreateWeekBodyDto extends OmitType(BaseWeekDto, [
    "status",
    "createdAt",
    "updatedAt",
    "deletedAt",
] as const) { }

@Exclude()
export class UpdateWeekBodyDto {
    @Expose()
    @IsNotEmpty()
    public name: string

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;
}


