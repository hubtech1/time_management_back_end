// import { MigrationInterface, QueryRunner, Table } from "typeorm";

// export class CreateDepartmentTable1655968556114 implements MigrationInterface {

//     public async up(queryRunner: QueryRunner): Promise<void> {
//         const table = new Table({
//             name: "department",
//             columns: [
//                 {
//                     name: "id",
//                     type: "uuid",
//                     isPrimary: true,
//                     isNullable: false
//                 },
//                 {
//                     name: "department_code",
//                     type: "int",
//                     isNullable: false
//                 },
//                 {
//                     name: "department_name",
//                     type: "varchar",
//                     isNullable: true
//                 },

//             ],
//         });

//         await queryRunner.createTable(table);

//     }

//     public async down(queryRunner: QueryRunner): Promise<void> {
//         await queryRunner.dropTable("department");
//     }

// }
