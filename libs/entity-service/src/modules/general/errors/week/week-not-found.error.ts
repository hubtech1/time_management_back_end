import { HttpException } from "@nestjs/common";


export class WeekNotFoundError extends HttpException {
    constructor() {
        super("Week not found!", 404)
    }
}