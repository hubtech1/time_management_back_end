import { DynamicModule, Module } from "@nestjs/common";
import { GraphQLModule } from "@nestjs/graphql";
import { ApolloDriver, ApolloDriverConfig } from "@nestjs/apollo";

import { env } from "@app/common/env";
import { Logger } from "@app/common";

@Module({})
export class GraphqlLoader {
    static load(): DynamicModule {
        if (!env.graphql.enabled) {
            return {
                module: GraphqlLoader,
            };
        }
        return {
            module: GraphqlLoader,
            imports: [
                GraphQLModule.forRoot<ApolloDriverConfig>({
                    driver: ApolloDriver,
                    playground: env.graphql.editor,
                    autoSchemaFile: "schema.gql",
                    cors: true,
                    path: env.graphql.route,
                    logger: new Logger("graphql"),
                }),
            ],
            exports: [GraphQLModule],
        };
    }
}
