# Tendermint Integration

---

> References:
>
> https://docs.tendermint.com/
>
> https://docs.tendermint.com/master/rpc/



## Kiến trúc deploy Tendermint hiện tại

![](../assets/images/tendermint-deploy-temp.jpg)

Hiện tại 3 node đều được Farmhub tự deploy lên GCP:

- https://tendermint-rpc-node-0.farmhub.asia/
- https://tendermint-rpc-node-1.farmhub.asia/
- https://tendermint-rpc-node-2.farmhub.asia/


![image-20210901150044689](../assets/images/image-20210901150044689.png)



## Cách hoạt động

Blockchain service này sẽ thực hiện đẩy data sang Tendermint nodes khi:

- 1 `Diary` mới được Tạo / Update / Delete. Nếu push không thành công thì sẽ lưu lại các item failed vào 1 bảng

- 1 Scheduled Cron Job sẽ chạy mỗi ngày để push các failed item lên lại Tendermint nodes. 

## Kỹ thuật bên dưới

Các file định nghĩa và setup để connect đến Tendermint nodes:

```
src
├── api
│   ├── services
│   │   ├── blockchain
│   │   │		tendermint.service.ts
│   │   ├── cruds
|	|	│		diary.service.ts
├── env.ts
```

1. Config

   Config của tendermint sẽ được quy định ở file `src/env.ts` load từ `environment variables` lên:

   ```typescript
   // ...
   blockchain: {
       uris: getOsEnv('TENDERMINT_URIS').split(',').map(uri => uri.trim()),
   },
   // ...
   ```

   

2. Implementation

   - Định nghĩa cấu trúc transaction

     ```typescript
     const tx = {
         type: 'DIARY',
         action, // CREATE | UPDATE | DELETE
         data: diary,
         author: {
             id: author.id,
             name: author.firstName + ' ' + author.lastName,
         },
     };
     ```

   - Encode base64

     ```typescript
     const txString = JSON.stringify(tx);
     const base64Tx = tx: Buffer.from(txString, 'utf8').toString('base64')
     ```

   - Gửi data bằng axios đến URL của một node ngẫu nhiên

     ```typescript
     const { data: txResult } = await this.retryPromise(
         this.axios.post<TendermintRpcResponse>(TendermintService.getRandomUri(), {
             jsonrpc: '2.0',
             method: 'broadcast_tx_commit',
             params: {
                 tx: base64Tx
             },
         }),
         env.blockchain.uris.length
     );
     ```

   - Xử lý response và update thông tin lại cho diary

     ```typescript
     const {
         result: {
             check_tx: {
                 code: checkCode = 1,
                 log: checkLog = 'check error',
             } = {},
             deliver_tx: {
                 code: deliverCode = 1,
                 log: deliverLog = 'deliver error',
             } = {},
             hash = '',
             height = '',
         } = {},
     } = txResult;
     
     if (checkCode > 0 || deliverCode > 0) {
         // Bao loi
         throw new DiaryHashPushError(checkCode > 0 ? checkLog : deliverLog);
     }
     
     diary.txHash = hash;
     diary.txHeight = +height;
     await this.repository.update(diary.id, diary);
     
     return diary;
     ```

   - Câu trúc của Tendermint RPC response có dạng là

     ```json
     {
         "jsonrpc": "2.0",
         "result": {
             // ...
             "check_tx": {
                 "code": "0 | 1", // 0 - success, >0 - failed
                 log: "string"
             },
             "deliver_tx": {
                 "code": "0 | 1", // 0 - success, >0 - failed
                 log: "string"
             },
             hash: "HEX_STRING",
             height: 123465 // number
         }
     }
     ```

     

   

