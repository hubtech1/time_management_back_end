import { MigrationInterface, QueryRunner, TableForeignKey } from "typeorm";

export class AddRelationForDateTable1656035881819 implements MigrationInterface {

    private toWeek = new TableForeignKey({
        name: "fk_date_week",
        columnNames: ["week_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "week"
    })

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createForeignKey("date", this.toWeek)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey("date", this.toWeek)
    }

}
