import { Factory, Seeder, times } from "typeorm-seeding";
import { Connection, In } from "typeorm";
import * as Faker from "faker";
import { Department } from "@app/entity-service/modules/general/entities";

export class CreateDepartment implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const factories: Department[] = [];

        await times(1, async () => {
            factories.push(
                await factory(Department)().make(),
            );
        });

        await connection.manager.save(factories, {
            chunk: 50,
        });
    }
}
