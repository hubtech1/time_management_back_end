import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, UseInterceptors } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";


import { DateRepository } from "../repositories";
import { DateNotFoundError } from "../errors";
import { DateService } from "../services";

import {
    ParseUtil,
    ListDataDto,
    BaseQueryDto,
    FullQueryDto,
    ErrorResponseDto,
    ListResponseInterceptor,
    OnUndefinedInterceptor,
    WhereQueryDto,
} from "@app/common";
import { CreateDateBodyDto, DateResponseDto, UpdateDateBodyDto } from "../dto";
import { Date } from "../entities";
import { Authorized } from "type-graphql";
import { IndexMetadata } from "typeorm/metadata/IndexMetadata";


@ApiTags("dates")
@Controller("/dates")
@ApiResponse({ type: ErrorResponseDto, status: 401 })
@ApiResponse({ type: ErrorResponseDto, status: 404 })
@ApiResponse({ type: ErrorResponseDto, status: 500 })
export class DateController {
    constructor(
        private dateService: DateService,
        private parseUtil: ParseUtil,
        private dateRepository: DateRepository,

    ) { }

    // Lấy hết danh sách Date
    @Get()
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated', 
        `
    })
    @UseInterceptors(ListResponseInterceptor)
    @OnUndefinedInterceptor(DateNotFoundError)
    @ApiResponse({ type: DateResponseDto, isArray: true, status: 200 })
    public find(
        @Query() query: FullQueryDto,

    ): Promise<ListDataDto<Date> | undefined> {
        const queryParse = this.parseUtil.fullQueryParam(query)
        return this.dateService.find(queryParse)
    }

    // Tạo thêm Date
    // @Authorized()
    @Post()
    @ApiOperation({
        description: `
        role: 0: 'staff', 1: 'manager', 2: 'director'`,
    })
    @OnUndefinedInterceptor(DateNotFoundError)
    @ApiResponse({ type: DateResponseDto, status: 200 })
    public async create(
        @Body() body: CreateDateBodyDto,
    ): Promise<Date | undefined> {
        return this.dateService.create(body)
    }

    // Tìm 1 Date theo id
    @Get("/:id")
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated',
        `
    })
    @OnUndefinedInterceptor(DateNotFoundError)
    @ApiResponse({ type: DateResponseDto, status: 200 })
    public async findOne(
        @Param("id") id: string,
        @Query() query: BaseQueryDto,
    ): Promise<Date | undefined> {
        const queryParse = this.parseUtil.baseQueryParam(query);
        return this.dateService.findOne(id, queryParse)
    }

    // Update thông tin Date

    @Authorized()
    @Patch("/:id")
    @OnUndefinedInterceptor(DateNotFoundError)
    @ApiResponse({ type: DateResponseDto, status: 200 })
    public async update(
        @Param("id") id: string,
        @Body() body: UpdateDateBodyDto
    ): Promise<Date | undefined> {
        const bodyParse = this.parseUtil.removeUndefinedProperty(body);
        return this.dateService.update(id, bodyParse)
    }

    // Xóa Date
    @Authorized()
    @Delete("/:id")
    @OnUndefinedInterceptor(DateNotFoundError)
    public async delete(
        @Param("id") id: string,
    ): Promise<Date | undefined> {
        return this.dateService.delete(id)
    }













}