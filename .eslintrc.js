module.exports = {
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint/eslint-plugin", "prettier", "import"],
    extends: ["plugin:@typescript-eslint/recommended", "plugin:import/recommended", "plugin:import/typescript"],
    root: true,
    env: {
        node: true,
        jest: true,
        es6: true,
    },
    rules: {
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/ban-types": "off",
        "prettier/prettier": "warn",
        "@typescript-eslint/ban-ts-comment": "off",
        "import/order": [
            "warn",
            {
                "newlines-between": "always",
            },
        ],
        "@typescript-eslint/no-unused-vars": ["warn", { args: "none" }],
    },
    settings: {
        "import/internal-regex": "^@app/",
        "import/resolver": {
            node: {},
            typescript: {},
        },
    },
};
