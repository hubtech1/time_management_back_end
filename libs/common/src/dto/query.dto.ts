import { IsNotEmpty, IsNumber, IsOptional, IsPhoneNumber, IsString } from "class-validator";
import { ArgsType, Field } from "@nestjs/graphql";
import { Type } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

@ArgsType()
export class WhereQueryDto {
    @IsString()
    @IsOptional()
    @ApiProperty({
        description: `
    Unlimited level nested,
    Special where: 
    eq => equal
    lt => less than
    gt => greater than
    ge => greater or equal
    le => less or equal
    not => not equal
    in => in array
    nin => not in array
    s => normal search
    fs => full text search
    inull => is null
    innull => is not null
    Example:
    "id": { "nin": ["uuid1", "uuid2"] } (result will have id except ["uuid1", "uuid2"])
    "price": { "gt": 0 } (result will have price > 0)
    `,
        nullable: true,
    })
    @Field({ description: "Unlimited level nested", nullable: true })
    public where?: string;
}

@ArgsType()
export class BaseQueryDto extends WhereQueryDto {
    @IsString()
    @ApiProperty({
        description: `
    Choose exactly what frontend need
    Conflict table name: Get previous table
    `,
        nullable: true,
    })
    @Field({ description: "Choose exactly what frontend need", nullable: true })
    @IsOptional()
    public select?: string;

    @IsString()
    @ApiProperty({ description: "Maximum 4 level nested", nullable: true })
    @Field({ description: "Maximum 4 level nested", nullable: true })
    @IsOptional()
    public relations?: string;

    @IsString()
    @ApiProperty({ description: "Maximum 2 level nested", nullable: true })
    @Field({ description: "Maximum 2 level nested", nullable: true })
    @IsOptional()
    public flatten?: string;
}

@ArgsType()
export class FullQueryDto extends BaseQueryDto {
    @IsNumber()
    @IsOptional()
    @ApiProperty({ nullable: true, default: 1 })
    @Field({ defaultValue: 1, nullable: true })
    @Type(() => Number)
    public page?: number;

    @IsNumber()
    @IsOptional()
    @ApiProperty({ nullable: true, default: 10 })
    @Field({ defaultValue: 10, nullable: true })
    @Type(() => Number)
    public take?: number;

    @IsNumber()
    @IsOptional()
    @ApiProperty({ nullable: true, default: 0 })
    @Field({ nullable: true })
    @Type(() => Number)
    public skip?: number;

    @IsString()
    @IsOptional()
    @ApiProperty({
        description: `
    Nested schema: { "table.property": value }
    Normal schema: { "property": value }
    table: Target table you want to sort
    property: Property in target table (Must be select in select params)
    value: 0: DESC, 1: ASC
    `,
        nullable: true,
    })
    @Field({ nullable: true, description: "Sort condition in JSON string" })
    public order?: string;
}

@ArgsType()
export class FollowDomainQueryDto extends BaseQueryDto {
    @IsString()
    @IsOptional()
    @ApiProperty({ nullable: true, default: "https://farmhub.asia" })
    @Field({ defaultValue: "https://farmhub.asia", nullable: true })
    @Type(() => String)
    public domain?: string;
}

@ArgsType()
export class FreeProductWithPartnerQueryDto {
    @IsNumber()
    @IsOptional()
    @ApiProperty({ nullable: true, default: 10 })
    @Field({ defaultValue: 10, nullable: true })
    @Type(() => Number)
    public take?: number;

    @IsNumber()
    @IsOptional()
    @ApiProperty({ nullable: true, default: 0 })
    @Field({ nullable: true })
    @Type(() => Number)
    public page?: number;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ nullable: false })
    @Field({ nullable: false })
    @Type(() => String)
    public partnerID: string;

    @IsString()
    @IsOptional()
    @ApiProperty({ nullable: true })
    @Field({ nullable: true })
    @Type(() => String)
    public categoryID?: string;

    @IsString()
    @IsOptional()
    @ApiProperty({ nullable: true })
    @Field({ nullable: true })
    @Type(() => String)
    public name?: string;
}

@ArgsType()
export class PermissionWithPartnerQueryDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty({ nullable: false })
    @Field({ nullable: false })
    @Type(() => String)
    public partnerID: string;
}

export class PhoneNumberExistQueryDto {
    @IsNotEmpty()
    @IsPhoneNumber("VN")
    @ApiProperty({ nullable: false })
    @Field({ nullable: false })
    @Type(() => String)
    public phoneNumber: string;
}
