import { IsDateString, IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString, IsUrl, IsUUID, Length, Max, Min } from "class-validator";
import { Exclude, Expose } from "class-transformer";
import { OmitType } from "@nestjs/swagger";



@Exclude()
export class BaseDepartmentDto {
    @Expose()
    @IsNotEmpty()
    public departmentCode: number

    @Expose()
    @IsNotEmpty()
    @IsString()
    public departmentName: string

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;

    @Expose()
    @IsDateString()
    @IsOptional()
    public createdAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public updatedAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public deletedAt: string;



}

@Exclude()
export class DepartmentResponseDto extends BaseDepartmentDto {
    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public id: string;
}

export class CreateDepartmentBodyDto extends OmitType(BaseDepartmentDto, [
    "status",
    "createdAt",
    "updatedAt",
    "deletedAt",
] as const) { }

@Exclude()
export class UpdateDepartmentBodyDto {
    @Expose()
    @IsNotEmpty()
    public departmentCode: number

    @Expose()
    @IsNotEmpty()
    @IsString()
    public departmentName: string

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;
}


