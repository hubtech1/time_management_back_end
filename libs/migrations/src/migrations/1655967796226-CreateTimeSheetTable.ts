// import { MigrationInterface, QueryRunner, Table } from "typeorm";

// export class CreateTimeSheetTable1655967796226 implements MigrationInterface {

//     public async up(queryRunner: QueryRunner): Promise<void> {
//         const table = new Table({
//             name: "time_sheet",
//             columns: [
//                 {
//                     name: "id",
//                     type: "uuid",
//                     isPrimary: true,
//                     isNullable: false
//                 },
//                 {
//                     name: "staff_id",
//                     type: "uuid",
//                     isNullable: false
//                 },
//                 {
//                     name: "date_id",
//                     type: "uuid",
//                     isNullable: false
//                 },
//                 {
//                     name: "start_time",
//                     type: "timestamp",
//                     isNullable: false
//                 },
//                 {
//                     name: "time_spent",
//                     type: "int",
//                     isNullable: false
//                 },
//                 {
//                     name: "title",
//                     type: "varchar",
//                     isNullable: false
//                 },
//                 {
//                     name: "description",
//                     type: "varchar",
//                     isNullable: true
//                 },
//                 {
//                     name: "attach",
//                     type: "varchar",
//                     isNullable: true
//                 },
//             ],
//         });

//         await queryRunner.createTable(table);

//     }

//     public async down(queryRunner: QueryRunner): Promise<void> {
//         await queryRunner.dropTable("time_sheet");

//     }

// }
