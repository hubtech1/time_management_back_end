import { MigrationInterface, QueryRunner, TableForeignKey } from "typeorm";

export class AddRelationForTimeSheetTable1656038203329 implements MigrationInterface {

    private toStaff = new TableForeignKey({
        name: "fk_timesheet_staff",
        columnNames: ["staff_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "staff"
    })

    private toDate = new TableForeignKey({
        name: "fk_timesheet_date",
        columnNames: ["date_id"],
        referencedColumnNames: ["id"],
        referencedTableName: "date"
    })

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createForeignKeys("time_sheet", [this.toStaff, this.toDate])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKeys("time_sheet", [this.toStaff, this.toDate])
    }

}
