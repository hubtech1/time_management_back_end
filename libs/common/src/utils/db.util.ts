import {
    EntityTarget,
    FindOneOptions,
    getConnectionOptions,
    LessThan,
    MoreThan,
    QueryRunner,
    Repository,
} from "typeorm";
import { normalize } from "normalize-diacritics";
import flat from "flat";
import { format } from "date-fns";

import { ListDataDto } from "../dto";
import { CrudOption } from "../interfaces";

import { ConditionQueryBuilder } from "./index";

import { env } from "@app/common/env";

export class DBUtil {
    public static async combineFirstCharacterAndLastWord(name: string): Promise<string | undefined> {
        if (!name || name.length === 0) {
            return undefined;
        }

        const words = name
            .trim()
            .split(" ")
            .filter((word) => word);
        const length = words.length;

        if (length === 0) {
            return undefined;
        } else {
            let result = "";

            for (let i = 0; i < length - 1; ++i) {
                result += (await normalize(words[i][0])).toUpperCase();
            }

            result += (await normalize(words[length - 1])).toUpperCase();

            return result;
        }
    }

    public static async getLoadedConnectionOptions(): Promise<any> {
        const loadedConnectionOptions = await Promise.resolve(getConnectionOptions());

        return Object.assign(loadedConnectionOptions, {
            type: env.db.type as any, // See createConnection options for valid types
            host: env.db.host,
            port: env.db.port,
            username: env.db.username,
            password: env.db.password,
            database: env.db.database,
            synchronize: env.db.synchronize,
            logging: env.db.logging,
            entities: env.app.dirs.entities,
            migrations: env.app.dirs.migrations,
        });
    }

    public async getValidCodeInTransaction(
        queryRunner: QueryRunner,
        entityClass: EntityTarget<any>,
        options: FindOneOptions = {},
        prefix: string,
        autoFill = 4,
    ): Promise<string> {
        const entity = await queryRunner.manager.findOne(entityClass, options);

        if (!entity) {
            return `${prefix}-${"1".padStart(autoFill, "0")}`;
        }

        const parts = entity.code.split("-");
        const index = +parts[parts.length - 1] + 1;

        return `${prefix}-${index.toString().padStart(autoFill, "0")}`;
    }

    public async findAndCount(
        repository: Repository<any>,
        option: CrudOption = {},
    ): Promise<ListDataDto<any> | undefined> {
        /**
         * Because condition parser will be made some `JOIN` and `SELECT` query inside
         * So I need to customize the query builder to handle the condition logic
         * Example: `{product: {price: {gt: 100}}}` will `JOIN` `product` table and `SELECT` `price` column
         */

        const query = ConditionQueryBuilder.ofRepository(repository)
            .relations(option.relations)
            .select(option.select)
            .addSelect(option.select)
            .condition(option.where)
            .order(option.order)
            .take(option.take)
            .skip(option.skip)
            .build();


        const [list, count] = await query.getManyAndCount();
        // TODO: update when item is array
        list.forEach((item) => {
            option.flatten?.forEach((string) => {
                const [flatten, flattenLv1] = string.split(".");

                if (flattenLv1) {
                    item[flatten][flattenLv1] = flat(item[flatten][flattenLv1]);
                } else {
                    item[flatten] = flat(item[flatten]);
                }
            });
        });




        return { list, count };
    }

    public async find(repository: Repository<any>, option: CrudOption = {}): Promise<any | undefined> {
        /**
         * Because condition parser will be made some `JOIN` and `SELECT` query inside
         * So I need to customize the query builder to handle the condition logic
         * Example: `{product: {price: {gt: 100}}}` will `JOIN` `product` table and `SELECT` `price` column
         */
        const query = ConditionQueryBuilder.ofRepository(repository)
            .relations(option.relations)
            .select(option.select)
            .addSelect(option.select)
            .condition(option.where)
            .order(option.order)
            .take(option.take)
            .skip(option.skip)
            .build();

        const list = await query.getMany();

        // TODO: update when item is array
        list.forEach((item) => {
            option.flatten?.forEach((string) => {
                const [flatten, flattenLv1] = string.split(".");

                if (flattenLv1) {
                    item[flatten][flattenLv1] = flat(item[flatten][flattenLv1]);
                } else {
                    item[flatten] = flat(item[flatten]);
                }
            });
        });

        return list;
    }

    public async count(repository: Repository<any>, option: CrudOption = {}): Promise<number | undefined> {
        /**
         * Because condition parser will be made some `JOIN` and `SELECT` query inside
         * So I need to customize the query builder to handle the condition logic
         * Example: `{product: {price: {gt: 100}}}` will `JOIN` `product` table and `SELECT` `price` column
         */
        const query = ConditionQueryBuilder.ofRepository(repository)
            .relations(option.relations)
            .select(option.select)
            .addSelect(option.select)
            .condition(option.where)
            .order(option.order)
            .take(option.take)
            .skip(option.skip)
            .build();

        const count = await query.getCount();

        return count;
    }

    public async findOne(repository: Repository<any>, id: string, option: CrudOption = {}): Promise<any | undefined> {
        const query = ConditionQueryBuilder.ofRepository(repository)
            .one(id)
            .relations(option.relations)
            .select(option.select)
            .addSelect(option.select)
            .condition(option.where)
            .build();

        const item = await query.getOne();

        // TODO: update when item is array
        item &&
            option.flatten?.forEach((string) => {
                const [flatten, flattenLv1] = string.split(".");
                if (flattenLv1) {
                    item[flatten][flattenLv1] = flat(item[flatten][flattenLv1]);
                } else {
                    item[flatten] = flat(item[flatten]);
                }
            });

        return item;
    }

    public static MoreThanDate(date: Date) {
        return MoreThan(format(date, "yyyy-MM-dd HH:mm:SS"));
    }
    public static LessThanDate(date: Date) {
        return LessThan(format(date, "yyyy-MM-dd HH:mm:SS"));
    }
}
