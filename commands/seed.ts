import * as path from "path";
import * as fs from "fs";

import chalk from "chalk";
import { program } from "commander";
import { createConnection, getConnectionOptions, runSeeder, useSeeding } from "typeorm-seeding";

// Cli helper.ts
const commander = program
    .version("1.0.0")
    .description("Run database seeds of your project")
    .option("-L, --logging", "enable sql query logging")
    .option("--factories <path>", "add filepath for your factories")
    .option("--seeds <path>", "add filepath for your seeds")
    .option("--run <seeds>", "run specific seeds (file names without extension)", (val) => val.split(","))
    .option("--config <file>", "path to your ormconfig.json file (must be a json)")
    .parse(process.argv)
    .opts();

// Get cli parameter for a different factory path
const factoryPath = commander.factories ? commander.factories : "libs/migrations/src/factories";

// Get cli parameter for a different seeds path
const seedsPath = commander.seeds ? commander.seeds : "libs/migrations/src/seeds/create";

// Get a list of seeds
const listOfSeeds = commander.run ? commander.run.map((l) => l.trim()).filter((l) => l.length > 0) : [];

// Search for seeds and factories
const run = async () => {
    const log = console.log;
    await useSeeding();

    let factoryFiles;
    let seedFiles: string[];
    try {
        factoryFiles = fs.readdirSync(factoryPath);
        seedFiles = fs.readdirSync(seedsPath, { encoding: "utf-8" }).map((file) => path.resolve(seedsPath, file));
    } catch (error) {
        return handleError(error);
    }

    // Filter seeds
    if (listOfSeeds.length > 0) {
        seedFiles = seedFiles.filter((sf) => listOfSeeds.indexOf(path.basename(sf).replace(".ts", "")) >= 0);
    }

    // Status logging to print out the amount of factories and seeds.
    log(chalk.bold("seeds"));
    log(
        "🔎 ",
        chalk.gray.underline(`found:`),
        chalk.blue.bold(
            `${factoryFiles.length} factories`,
            chalk.gray("&"),
            chalk.blue.bold(`${seedFiles.length} seeds`),
        ),
    );

    // Get database connection and pass it to the seeder
    try {
        const options = await getConnectionOptions();
        const connection = await createConnection(options);
    } catch (error) {
        return handleError(error);
    }

    // Show seeds in the console
    for (const seedFile of seedFiles) {
        try {
            let className = seedFile.split("/")[seedFile.split("/").length - 1];
            className = className.replace(".ts", "").replace(".js", "");
            className = className.split("-")[className.split("-").length - 1];
            log("\n" + chalk.gray.underline(`executing seed:  `), chalk.green.bold(`${className}`));
            const time = Date.now();
            const seedFileObject = await import(seedFile);
            await runSeeder(seedFileObject[className]);
            const endTime = Math.trunc((Date.now() - time) / 1000);
            log(chalk.yellow.italic(`\t executed time: ${endTime}s`));
        } catch (error) {
            console.error("Could not run seed ", error);
            process.exit(1);
        }
    }

    log("\n👍 ", chalk.gray.underline(`finished seeding`));
    process.exit(0);
};

const handleError = (error) => {
    console.error(error);
    process.exit(1);
};

run();
