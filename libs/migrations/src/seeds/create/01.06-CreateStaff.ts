import { Factory, Seeder, times } from "typeorm-seeding";
import { Connection, In } from "typeorm";
import * as Faker from "faker";
import { Staff } from "@app/entity-service/modules/general/entities";
import { DepartmentRepository } from "@app/entity-service/modules/general/repositories";

export class CreateStaff implements Seeder {
    public departmentRepository: DepartmentRepository
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const factories: Staff[] = [];
        // const departmentRepo = new DepartmentRepository()
        // departmentRepository: DepartmentRepository

        await times(1, async () => {
            factories.push(
                await factory(Staff)().make(),
            );
        });

        await connection.manager.save(factories, {
            chunk: 50,
        });
    }
}
