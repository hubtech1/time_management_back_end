import { HttpException } from "@nestjs/common";


export class DateNotFoundError extends HttpException {
    constructor() {
        super("Date not found!", 404)
    }
}