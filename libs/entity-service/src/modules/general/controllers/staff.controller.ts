import { Body, Controller, Delete, Get, Param, Patch, Post, Put, Query, Request, UseInterceptors } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";


import { StaffRepository } from "../repositories";
import { StaffNotFoundError } from "../errors";
import { StaffService } from "../services";

import {
    ParseUtil,
    ListDataDto,
    BaseQueryDto,
    FullQueryDto,
    ErrorResponseDto,
    ListResponseInterceptor,
    OnUndefinedInterceptor,
    WhereQueryDto,
} from "@app/common";
import { CreateStaffBodyDto, StaffResponseDto, UpdateStaffBodyDto } from "../dto";
import { Staff } from "../entities";
import { Authorized, UseMiddleware } from "type-graphql";
import { IndexMetadata } from "typeorm/metadata/IndexMetadata";


@ApiTags("")
@Controller("/")
@ApiResponse({ type: ErrorResponseDto, status: 401 })
@ApiResponse({ type: ErrorResponseDto, status: 404 })
@ApiResponse({ type: ErrorResponseDto, status: 500 })
export class StaffController {
    constructor(
        private staffService: StaffService,
        private parseUtil: ParseUtil,
        private staffRepository: StaffRepository,
    ) { }

    // test staff list no need token
    @Get("/stafftest")
    public findTest(
        @Query() query: FullQueryDto,

    ): Promise<ListDataDto<Staff> | undefined> {
        const queryParse = this.parseUtil.fullQueryParam(query)
        return this.staffService.find(queryParse)
    }


    // Lấy hết danh sách Staff
    @Get("/staffs")
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated', 
        `
    })
    @UseInterceptors(ListResponseInterceptor)
    @OnUndefinedInterceptor(StaffNotFoundError)
    @ApiResponse({ type: StaffResponseDto, isArray: true, status: 200 })
    public find(
        @Query() query: FullQueryDto,

    ): Promise<ListDataDto<Staff> | undefined> {
        const queryParse = this.parseUtil.fullQueryParam(query)
        return this.staffService.find(queryParse)
    }

    // Đăng ký
    @Post("/register")
    @ApiOperation({
        description: `
        role: 0: 'staff', 1: 'manager', 2: 'director'`,
    })
    @OnUndefinedInterceptor(StaffNotFoundError)
    @ApiResponse({ type: StaffResponseDto, status: 200 })
    public async create(
        @Body() body: CreateStaffBodyDto,
    ): Promise<Staff | undefined> {
        return this.staffService.create(body)
    }

    // Login 
    @Post("/login")
    @ApiOperation({
        description: `
        role: 0: 'staff', 1: 'manager', 2: 'director'`,
    })
    @OnUndefinedInterceptor(StaffNotFoundError)
    @ApiResponse({ type: StaffResponseDto, status: 200 })
    public async login(@Request() req: any) {
        const { staffCode, password } = req.body;

        return this.staffService.validateStaff(staffCode, password)
    }

    // Tìm 1 staff theo staff code
    @Get("/staffs/:id")
    @ApiOperation({
        description: `
        status: 0: 'activated', 1: 'deactivated',
        role: 0: 'staff', 1: 'manager', 2: 'director', 
        `
    })
    @OnUndefinedInterceptor(StaffNotFoundError)
    @ApiResponse({ type: StaffResponseDto, status: 200 })
    public async findStaffCode(
        @Param("id") staffCode: string,
        @Query() query: BaseQueryDto,
    ): Promise<Staff | undefined> {
        return this.staffService.findStaffCode(staffCode)
    }

    // Update thông tin nhân viên
    @Patch("/staffs/:id")
    @OnUndefinedInterceptor(StaffNotFoundError)
    @ApiResponse({ type: StaffResponseDto, status: 200 })
    public async update(
        @Param("id") staffCode: string,
        @Body() body: UpdateStaffBodyDto
    ): Promise<Staff | undefined> {
        const bodyParse = this.parseUtil.removeUndefinedProperty(body);
        return this.staffService.update(staffCode, bodyParse)
    }

    // Xóa nhân viên
    @Delete("/staffs/:id")
    @OnUndefinedInterceptor(StaffNotFoundError)
    public async delete(
        @Param("id") staffCode: string,
    ): Promise<Staff | undefined> {
        return this.staffService.delete(staffCode)
    }

}