import { AuthGuard } from "@nestjs/passport";
import { applyDecorators, UseGuards } from "@nestjs/common";
import { ApiBearerAuth } from "@nestjs/swagger";

export class IdentityV2Guard extends AuthGuard("identity-v2") {}

export const Authorized = () => applyDecorators(UseGuards(IdentityV2Guard), ApiBearerAuth("identity-v2"));
