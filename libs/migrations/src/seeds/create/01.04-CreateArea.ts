import { Factory, Seeder, times } from "typeorm-seeding";
import { Connection, In } from "typeorm";
import * as Faker from "faker";
import { Area } from "@app/entity-service/modules/general/entities";

export class CreateArea implements Seeder {
    public async run(factory: Factory, connection: Connection): Promise<any> {
        const factories: Area[] = [];

        await times(10, async () => {
            factories.push(
                await factory(Area)().make(),
            );
        });

        await connection.manager.save(factories, {
            chunk: 50,
        });
    }
}
