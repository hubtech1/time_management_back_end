import { Column, Entity as EntityTypeORM, ManyToOne, OneToMany } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";

import { Base } from "@app/common/entities";
import { Department } from "./department.entity";
import { TimeSheet } from "./time_sheet.entity";


@EntityTypeORM()
@ObjectType()
export class Staff extends Base {
    @Column({ name: "staff_code", nullable: false })
    @Field()
    public staffCode: string;

    @Column({ name: "full_name", nullable: false })
    @Field()
    public fullName: string;

    @Column({ name: "role", nullable: false })
    @Field()
    public role: number;

    @Column({ name: "avatar", nullable: true })
    @Field()
    public avatar: string;

    @Column({ name: "password", nullable: true })
    @Field()
    public password: string;

    @Column({ name: "email", nullable: true })
    @Field()
    public email: string;

    @ManyToOne(type => Department, department => department.departmentId)
    public departmentId: string

    @OneToMany(() => TimeSheet, timeSheet => timeSheet.staffId)
    public staffId: string
}