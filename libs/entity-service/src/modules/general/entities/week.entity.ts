import { Column, Entity as EntityTypeORM, OneToMany } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";

import { Base } from "@app/common/entities";
import { Date } from "./date.entity";


@EntityTypeORM()
@ObjectType()
export class Week extends Base {

    @Column({ name: "name", nullable: false })
    @Field()
    public name: string;

    @OneToMany(() => Date, date => date.weekId)
    public weekId: string

}