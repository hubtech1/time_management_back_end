# API Caching

---

> Tham khảo: [https://blog.logrocket.com/caching-strategies-to-speed-up-your-api/](https://blog.logrocket.com/caching-strategies-to-speed-up-your-api/)

---

API caching là một một phương pháp cache lại dữ liệu trả về của các request API giống nhau.
Tăng tốc độ truy cập và giảm tải cho database

|                |                                   |
| :------------- | --------------------------------: |
| Không có cache |        ![No Cache](../assets/images/api-cache.jpg) |
| Có cache:      | ![No Cache](../assets/images/api-cache-enable.jpg) |

## Cấu trúc API Cache trong code

Mô tả đường đi của request trong source code:

![Code Diagram](../assets/images/api-cache-code-diagram.jpg)

-   Trường hợp MISS (url không có trong cache): API thực hiện bình thường (4 bước)

    -   Check cache -> không có
    -   Lấy dữ liệu từ DB
    -   Save vào cache (với timeout)
    -   Trả về User

-   Trường hợp HIT (có url trong cache) (3 bước)
    -   Check cache -> có
    -   Trả dữ liệu về user
    -   Cập nhật timeout cho cache

**Như vậy**, API caching sẽ hiệu quả khi 1 API được call nhiều lần.
Nếu API được ít request đến, thời gian check cache và redis sẽ làm chậm request hơn

## Cài đặt trong code

```
src/
|__api/
  |__middlewares/
    |__CacheResponseMiddlewares.ts
```

> Tham khảo: [https://github.com/typestack/routing-controllers#using-middlewares](https://github.com/typestack/routing-controllers#using-middlewares)

-   Implementation

    -   Check cache khi vào controller

    ```typescript
    async function useBefore(
        opts: CacheOptions,
        request: Request,
        response: Response,
        next: NextFunction
    ): Promise<void> {
        if (!Container.has(CACHE_MANAGER)) {
            return next();
        }

        const cache = Container.get(CACHE_MANAGER);

        if (!cache) {
            return next();
        }
        try {
            const url = request.url;
            const cacheResponse = await cache.get<string>(url);
            if (!cacheResponse) {
                winstonLogger.debug('[Cache]: Miss' + url);
                return next();
            }
            winstonLogger.debug('[Cache] Hit: ' + url);
            response.status(200).json(cacheResponse);
            return;
        } catch (e: any) {
            return next();
        }
    }
    ```

    - Save cache sau khi trả về

    ```typescript
    async function useAfter(opts: CacheOptions, action: Action, content: any): Promise<any> {
        if (!Container.has(CACHE_MANAGER)) {
            return content;
        }

        if (action.response.statusCode >= 400) {
            return content;
        }
        const cache = Container.get(CACHE_MANAGER);
        if (cache) {
            cache.set(action.request.url, content, {
                ttl: opts.ttl,
            })
            .then(() => {
                winstonLogger.debug('[Cache] saved: ' + action.request.url);
            })
            .catch((error) => {
                winstonLogger.debug('[Cache] error: ' + action.request.url + ' Error: ' + error);
            });
        }

        return content;
    }
    ```

    - Kết hợp lại
    
    ```typescript
    export function UseCache(opts: CacheOptions = {}): MethodDecorator {
        return (target, propKey, descriptor) => {
            if (!env.redis.caching) {
                return;
            }
            UseBefore(useBefore.bind(useBefore, opts))(target, propKey, descriptor);
            UseInterceptor(useAfter.bind(useAfter, opts))(target, propKey, descriptor);
            if (opts.browser_ttl) {
                Header('Cache-Control', `max-age=${Math.min(60, opts.browser_ttl)}`)(
                    target,
                    propKey,
                    descriptor
                );
            }
        };
    }
    ```

-   Cách sử dụng

Sử dụng `@UseCache()` trên đầu mỗi method của class controller

VD: file `src/controllers/category.controller.ts`

```typescript
class CategoryController {
    // ...
    @Get()
    
    @UseInterceptors(ListResponseInterceptor)
    @UseCache({ ttl: 1, browser_ttl: 1 })
    @OnUndefined(CategoryNotFoundError)
    @ApiResponse({ type: CategoryResponse, isArray: true })
    public find(
        @Query() query: FullQuery
    ): Promise<CListData<Category> | undefined> {
        const queryParse = this.parseHelper.fullQueryParam(query);

        return this.categoryService.find(queryParse);
    }
    // ...
}
```
