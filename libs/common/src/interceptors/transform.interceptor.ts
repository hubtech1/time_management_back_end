import { ClassConstructor, plainToClass } from "class-transformer";
import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { map } from "rxjs";

export function TransformInterceptor<T = any>(classFunc: ClassConstructor<T>): ClassConstructor<NestInterceptor> {
    @Injectable()
    class InsideTransformInterceptor implements NestInterceptor {
        intercept(context: ExecutionContext, next: CallHandler<any>) {
            return next.handle().pipe(
                map((data) => {
                    if (data.hasOwnProperty("list") && data.hasOwnProperty("count")) {
                        // paginate response
                        data.list = plainToClass(classFunc, data.list);
                        return data;
                    }
                    return plainToClass(classFunc, data.list);
                }),
            );
        }
    }

    return InsideTransformInterceptor;
}
