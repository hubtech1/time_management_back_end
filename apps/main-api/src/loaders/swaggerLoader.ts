import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";

import { env } from "@app/common";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const basicAuth = require("express-basic-auth");

export function useSwagger(app: INestApplication) {
    if (env.swagger.enabled) {
        app.use(
            env.swagger.route,
            basicAuth({
                users: {
                    [env.swagger.username]: env.swagger.password,
                },
                challenge: true,
            }),
        );

        const config = new DocumentBuilder()
            .setTitle(env.app.name)
            .setDescription(env.app.description)
            .setVersion(env.app.version)
            .addServer(`${env.app.schema}://${env.app.host}:${env.app.port}`)
            .addServer(`${env.app.schema}://${env.app.host}`)
            .addBearerAuth({ bearerFormat: "JWT", type: "http", in: "headers" }, "identity-v2")
            .build();

        const document = SwaggerModule.createDocument(app, config);

        SwaggerModule.setup(env.swagger.route, app, document, {
            swaggerOptions: {
                docExpansion: "none",
                persistAuthorization: true,
            },
        });
    }
}
