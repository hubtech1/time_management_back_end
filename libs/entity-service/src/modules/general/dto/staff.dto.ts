import { IsDateString, IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString, IsUrl, IsUUID, Length, Max, Min } from "class-validator";
import { Exclude, Expose } from "class-transformer";
import { OmitType } from "@nestjs/swagger";



@Exclude()
export class BaseStaffDto {
    @Expose()
    @IsNotEmpty()
    public staffCode: string

    @Expose()
    @IsNotEmpty()
    @IsString()
    public fullName: string

    @Expose()
    // @IsNotEmpty()
    public departmentId: string

    @Expose()
    @IsNotEmpty()
    @IsNumber()
    public role: number

    @Expose()
    public avatar: string

    @Expose()
    @IsNotEmpty()
    @Length(6, 20)
    public password: string

    @Expose()
    @IsNotEmpty()
    @IsEmail()
    public email: string

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;

    @Expose()
    @IsDateString()
    @IsOptional()
    public createdAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public updatedAt: string;

    @Expose()
    @IsDateString()
    @IsOptional()
    public deletedAt: string;



}

@Exclude()
export class StaffResponseDto extends BaseStaffDto {
    @Expose()
    @IsNotEmpty()
    @IsUUID()
    public id: string;
}

export class CreateStaffBodyDto extends OmitType(BaseStaffDto, [
    "status",
    "createdAt",
    "updatedAt",
    "deletedAt",
] as const) { }

@Exclude()
export class UpdateStaffBodyDto {
    @Expose()
    public staffCode: string

    @Expose()
    @IsString()
    public fullName: string

    @Expose()
    // @IsNotEmpty()
    public departmentId: string

    @Expose()
    // @IsNumber()
    @Min(0)
    @Max(2)
    public role: number

    @Expose()
    // @IsUrl()
    public avatar: string

    @Expose()
    @Length(8, 20)
    public password: string

    @Expose()
    @IsEmail()
    public email: string

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(0)
    @Max(1)
    public status: number;
}


