import { Column, Entity as EntityTypeORM, ManyToOne, OneToMany } from "typeorm";
import { Field, ObjectType } from "@nestjs/graphql";

import { Base } from "@app/common/entities";
import { Week } from "./week.entity";
import { TimeSheet } from "./time_sheet.entity";
import { StoreConfig } from "cache-manager";


@EntityTypeORM()
@ObjectType()
export class Date extends Base {
    // @Column({ name: "week_id", nullable: false })
    // @Field()
    // public weekId: string;

    @Column({ name: "date", nullable: false })
    @Field()
    public date: string;

    @ManyToOne(type => Week, week => week.weekId)
    public weekId: string

    @OneToMany(() => TimeSheet, timeSheet => timeSheet.dateId)
    public dateId: string

}